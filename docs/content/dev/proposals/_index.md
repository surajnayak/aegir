---
title: Proposals

---

1. [Optimize updates](updates): Updates incur too much downtime. Re-architecting our deployment strategy can resolve this.
