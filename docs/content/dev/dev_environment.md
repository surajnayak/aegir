---
title: Development Environment
menuTitle: "Dev environment"
weight: 10
---


## Getting started

### Cloning the repository

To get started, clone this repository locally.

```sh
git clone --recursive https://gitlab.com/aegir/aegir.git
cd aegir
```

### Installing prerequisites

Local development of Ægir is accomplished using [Vagrant](https://en.wikipedia.org/wiki/Vagrant_(software)) and [Virtualbox](https://en.wikipedia.org/wiki/VirtualBox).  Install them as you would other packages on your OS.

```sh
sudo apt install vagrant virtualbox
```

### Setting up a local instance

Now that the required prerequisites are installed, the first step is to build a local Vagrant box.

1. Bootstrap [Drumkit](http://drumk.it/).  Note the *space*, as we're **sourcing** this file, in order to load some environment variable, etc.
    * `. d`
1. Build VM images.
    * `make boxes`
1. Once the boxes are built, you can start up a VM, and install Ægir easily.  This will provision a local development site.
    * `vagrant up`
1. Once the VM is up, and provisioning is complete, you'll need to add an entry in your `/etc/hosts`.
    * `192.168.99.201 aegir.vm www.example.com www.example.net www.example.org`
1. With that done, visit [http://aegir.vm](http://aegir.vm), and login using:
    * **Username**: `admin`
    * **Password**: `pwd`

### Working on development issues

Check out the feature branch that you want to work on.

```sh
git checkout 6-base-profile-feature
```
