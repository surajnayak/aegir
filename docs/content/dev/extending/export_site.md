---
title: Export site type config
weight: 15

---

First, visit `/admin/config/development/features`, and select the "Configure Bundles" tab. We'll want to create a bundle to contain our Hugo-related Features. Let's call it "Ægir: Hugo".

Next, go back to the Features page, and click the "Create New Feature" button.We first need to create a "Hugo: Site" Feature to contain our new site type. We'll add operations at this level too.

The bundle will automatically name-space the exported Feature, so edit the machine-readable name to remove the leading `hugo_`, since it'd be duplicated with the bundle namespace.

Set the "Path" to `profiles/contrib/aegir/modules`. Select the bundle and the "Site type" component that we had created earlier.

Click "Write" to export the code/config.

