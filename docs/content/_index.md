---
title: Home
---

Welcome to Ægir
===============

Ægir is a framework for hosting and managing websites and other applications.

Ægir is built as a [Drupal](https://www.drupal.org) distribution, making it both easy to self-host, integrate with other tools and services, and extend with plugins, themes or custom code.

The version documented here is a full re-write using Drupal 8. It is still a prototype, and thus not yet fully functional. [Aegir 3.x](http://docs.aegirproject.org) remains the supported release for the foreseeable future.

Check out [http://aegir.hosting/](http://aegir.hosting/) for more information. Professional support, training, consulting and other services are offered by the [Ægir Cooperative](http://aegir.coop).

This documentation is designed primarily for the following audiences:

* [Users](usage): those looking to use Ægir to install and manage a website or other supported application.
* [Administrators](administration): those looking to host Ægir on their own infrastructure.
* [Developers](development): those looking to add new features, enhance or fix bugs in existing ones or otherwise extend Ægir.
