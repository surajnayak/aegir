<?php

namespace Drupal\aegir_queue\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\aegir_queue\TaskQueue\TaskQueueInterface;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;
use Drupal\Console\Core\Style\DrupalStyle;

/**
 * Class AegirEchoCommand.
 *
 * @package Drupal\aegir_queue
 *
 * @DrupalCommand (
 *     extension="aegir_queue",
 *     extensionType="module"
 * )
 */
class AegirEchoCommand extends Command {

  use ContainerAwareCommandTrait;

  /**
   * {@inheritdoc}
   */
  public function __construct(TaskQueueInterface $queue) {
    parent::__construct();
    $this->queue = $queue;
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('aegir:echo')
      ->setDescription($this->trans('commands.aegir.echo.description'))
      ->addArgument(
        'string',
        InputArgument::OPTIONAL,
        $this->trans('A string that will be echo\'d back by the queue worker.'),
        'echo'
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);

    $result = $this->queue->AddTask('aegird.echo', [$input->getArgument('string')]);

    while (!$result->isReady()) {
      usleep(100000);
      echo '.';
    }

    if ($result->isSuccess()) {
      $io->info($result->getResult());
    }
    else {
      // @codeCoverageIgnoreStart
      echo "ERROR";
      echo $result->getTraceback();
      // @codeCoverageIgnoreEnd
    }
  }

}
