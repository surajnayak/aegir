<?php

namespace Drupal\aegir_queue\Form;

use Drupal\aegir_queue\TaskQueue\TaskQueueInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\config_update\ConfigRevertInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class TaskQueueConfigForm.
 *
 * @package Drupal\aegir_queue
 */
class TaskQueueConfigForm extends ConfigFormBase {

  /**
   * We provide a simple method to revert configuration to default values.
   *
   * @var ConfigReverter
   */
  protected $reverter;

  /**
   * The task queue.
   *
   * @var Drupal\aegir_queue\TaskQueue\TaskQueueInterface
   */
  protected $queue;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ConfigRevertInterface $reverter, TaskQueueInterface $queue) {
    parent::__construct($config_factory);
    $this->reverter = $reverter;
    $this->queue = $queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the config factory service that we'll pass along to the parent
      // constructor.
      $container->get('config.factory'),
      // Load the config reverter service.
      $container->get('config_update.config_update'),
      // Load the task queue service.
      $container->get('aegir_queue.task_queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return $this->queue->getConfigNames();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aegir_queue_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['connection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Task queue connection settings'),
    ];
    foreach ($this->queue->getConnectorForm() as $name => $element) {
      $form['connection'][$name] = $element;
    };
    $form['connection']['check'] = [
      '#type' => 'submit',
      '#value' => $this->t('Check connection settings'),
    ];

    $form = parent::buildForm($form, $form_state);
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reset to defaults'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getTriggeringElement()['#id'] != 'edit-reset') {
      if ($this->queue->checkConnectorSettings($form_state) === FALSE) {
        $form_state->setErrorByName('connection', $this->t('Verify connection settings.'));
        return drupal_set_message($this->t('The configuration options have NOT been saved.'), 'error');
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    switch ($form_state->getTriggeringElement()['#id']) {
      case 'edit-reset':
        $this->resetForm();
        break;

      case 'edit-submit':
        $this->queue->saveConnectorSettings($form_state);
        parent::submitForm($form, $form_state);
        break;

      case 'edit-check':
        // Check occurs in formValidate().
      default:
        break;
    }
  }

  /**
   * Revert form values to defaults.
   */
  protected function resetForm() {
    foreach ($this->getEditableConfigNames() as $config_name) {
      if (!$this->reverter->revert('system.simple', $config_name)) {
        // @codeCoverageIgnoreStart
        return drupal_set_message(
          $this->t(
            'Failed to reset configuration (:name) to default values.',
            [':name' => $config_name]
          ), 'error');
        // @codeCoverageIgnoreEnd
      }
    }
    drupal_set_message($this->t('The configuration options have been reset to default values.'));
  }

}
