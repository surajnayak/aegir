<?php

namespace Drupal\aegir_queue\Connector;

use Drupal\Core\Form\FormStateInterface;

/**
 * Interface ConnectorInterface.
 *
 * @package Drupal\aegir_queue
 */
interface ConnectorInterface {

  /**
   * Returns the name of the configuration used by this connector.
   */
  public function getConfigName();

  /**
   * Returns the queue config form.
   */
  public function getForm();

  /**
   * Save configuration from the current form state.
   */
  public function saveConfigFromFormState(FormStateInterface $form_state);

  /**
   * Set current configuration from the current form state.
   */
  public function setConfigFromFormState(FormStateInterface $form_state);

}
