<?php

namespace Drupal\aegir_queue\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Test to exercise API functions not covered by other tests.
 *
 * @group aegir
 */
class ApiTest extends WebTestBase {

  public $profile = 'aegir';

  /**
   * Disable schema validation for the base theme.
   *
   * @see: https://www.drupal.org/node/2860072
   */
  protected function getConfigSchemaExclusions() {
    return array_merge(parent::getConfigSchemaExclusions(), ['bootstrap.settings']);
  }

  /**
   * Tests that the service instantiates the correct object.
   */
  public function testApiMethods() {

    $task_queue = \Drupal::service('aegir_queue.task_queue');

    $this->assertEqual(
      get_class($task_queue),
      'Drupal\\aegir_queue\\TaskQueue\\CeleryTaskQueue',
      'Ægir queue service object instantiated.'
    );

  }

}
