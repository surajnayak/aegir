<?php

namespace Drupal\aegir_queue\TaskQueue;

/**
 * Class TaskQueue.
 *
 * @package Drupal\aegir_queue
 */
class CeleryTaskQueue extends AbstractTaskQueue implements TaskQueueInterface {

  /**
   * Create an 'echo' task to confirm connectivity with the queue.
   */
  protected function testConnection() {
    if (!$task = $this->addTask('aegird.echo', [$this->t('The task queue is properly configured.')])) {
      return FALSE;
    }

    $increment = 100000;
    $counter = 0;
    $timeout = 5000000;
    while (!$task->isReady()) {
      usleep($increment);
      $counter += $increment;
      if ($counter >= $timeout) {
        $connection_timeout = $this->t('Task execution exceeded timeout of :timeout seconds.', [':timeout' => $timeout / 1000000]);
        drupal_set_message($connection_timeout, 'error');
        // TODO: inject a logger into all Aegir objects to allow something
        // like: $this->log->error('...').
        \Drupal::logger('aegir_queue')->error($connection_timeout);
        return FALSE;
      }
    }

    if ($task->isSuccess()) {
      drupal_set_message($task->getResult());
      drupal_set_message($this->t('Test task execution took :count seconds.', [':count' => round($counter / 1000000, 1)]));
    }
    else {
      // @codeCoverageIgnoreStart
      drupal_set_message($task->getTraceback(), 'error');
      // @codeCoverageIgnoreEnd
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addTask(string $type, array $args) {
    if ($connection = $this->connector->connect()) {
      return $connection->PostTask($type, $args);
    }
    return FALSE;
  }

}
