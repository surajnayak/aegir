@queue @queue-worker @api @disable-on-ci
Feature: Aegir task queue worker service
  In order to allow tasks to be run on servers,
  as an Aegir administrator,
  I need to be able to run the task queue runner service.

  Scenario: The task queue worker service is running.
    Given I run "service aegird status"
     Then I should get:
      """
      aegird.service - Aegir queue worker service
      Loaded: loaded (/etc/systemd/system/aegird.service; enabled; vendor preset: enabled)
      Active: active (running)
      """

  Scenario: The task queue worker can accept and process tasks.
    Given I am at "/var/aegir/platforms/aegir" on the command line
     When I run "drupal aegir:echo SomeTestString"
     Then I should get:
      """
      SomeTestString
      """
     When I run "drupal aegir:echo AnotherTestString"
     Then I should get:
      """
      AnotherTestString
      """
