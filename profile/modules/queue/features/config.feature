@queue @queue-config @api @disable-on-ci
Feature: Aegir task queue configuration
  In order to allow tasks to be securely added to the task queue,
  as an Aegir administrator,
  I need to be able to configure the task queue.

  Background:
    Given I am logged in as an "Ægir administrator"
      And I am on "admin/aegir/queue"

  Scenario: Confirm default task queue configuration.
     Then I should see the heading "Task queue configuration" in the "header" region
      And the "Host" field should contain "localhost"
      And the "Login" field should contain "guest"
      And the "Password" field should contain ""

  Scenario: Check invalid task queue configuration.
     When I fill in "Login" with "invalid"
      And I fill in "Password" with "invalid"
      And I press "Check connection settings"
     Then I should see the following error messages:
          | error messages                                                     |
          | Failed to connect to the task queue. See the log for more details. |
          | The configuration options have NOT been saved.                     |
          | Verify connection settings.                                        |
     Then I should not see the following success messages:
          | success messages                           |
          | The task queue is properly configured.     |
          | The configuration options have been saved. |

  Scenario: Ensure invalid task queue configuration is not saved.
     When I fill in "Login" with "invalid"
      And I fill in "Password" with "invalid"
      And I press "Save configuration"
     Then I should see the following error messages:
          | error messages                                                     |
          | Failed to connect to the task queue. See the log for more details. |
          | The configuration options have NOT been saved.                     |
          | Verify connection settings.                                        |
     Then I should not see the following success messages:
          | success messages                           |
          | The task queue is properly configured.     |
          | The configuration options have been saved. |

  Scenario: Check valid task queue configuration.
     When I fill in "Host" with "localhost"
      And I fill in "Login" with "guest"
      And I fill in "Password" with "guest"
      And I press "Check connection settings"
     Then I should not see the following error messages:
          | error messages                                                     |
          | Failed to connect to the task queue. See the log for more details. |
          | The configuration options have NOT been saved.                     |
          | Verify connection settings.                                        |
      And I should see the following success messages:
          | success messages                           |
          | The task queue is properly configured.     |
          | Test task execution took                   |
      And I should not see the following success messages:
          | success messages                           |
          | The configuration options have been saved. |

  Scenario: Change task queue configuration.
     When I fill in "Host" with "0.0.0.0"
      And I fill in "Login" with "guest"
      And I fill in "Password" with "guest"
      And I press "Save configuration"
     Then I should not see the following error messages:
          | error messages                                                     |
          | Failed to connect to the task queue. See the log for more details. |
          | The configuration options have NOT been saved.                     |
          | Verify connection settings.                                        |
      And I should see the following success messages:
          | success messages                           |
          | The task queue is properly configured.     |
          | The configuration options have been saved. |
          | Test task execution took                   |
      And the "Host" field should contain "0.0.0.0"
      And the "Login" field should contain "guest"
      And the "Password" field should contain ""

  Scenario: Reset task queue configuration.
     When I press "Reset to defaults"
     Then I should see the success message "The configuration options have been reset to default values."
      And the "Host" field should contain "localhost"
      And the "Login" field should contain "guest"
      And the "Password" field should contain ""


