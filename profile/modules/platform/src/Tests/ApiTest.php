<?php

namespace Drupal\aegir_platform\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\aegir_platform\Entity\Entity as Platform;

/**
 * Test to exercise API functions not covered by other tests.
 *
 * @group aegir
 */
class ApiTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'aegir_test_platform',
  ];

  public $profile = 'aegir';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Disable schema validation for the base theme.
   *
   * @see: https://www.drupal.org/node/2860072
   */
  protected function getConfigSchemaExclusions() {
    return array_merge(parent::getConfigSchemaExclusions(), ['bootstrap.settings']);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([], 'TestUser');
    $this->user->addRole('aegir_platform_manager');
    $this->user->save();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testApiMethods() {
    $settings = [
      // Use one of the bundles exported to the 'aegir_test_platform' module.
      'type' => 'test_platform_type_1',
      'name' => 'TEST_PLATFORM_A',
      'created' => time(),
      'user_id' => $this->rootUser->id(),
      'status' => 1,
    ];
    $platform = Platform::create($settings);

    $type = $platform->getType();
    $this->assertEqual($type, $settings['type'], 'Get type (bundle) of Ægir platform.');

    $name = $platform->getName();
    $this->assertEqual($name, $settings['name'], 'Get name of Ægir platform.');

    $new_name = 'TEST_PLATFORM_B';
    $platform->setName($new_name);
    $name = $platform->getName();
    $this->assertEqual($name, $new_name, 'Set name of Ægir platform.');

    $created = $platform->getCreatedTime();
    $this->assertEqual($created, $settings['created'], 'Get creation time of Ægir platform.');

    $new_created = time() - 10;
    $platform->setCreatedTime($new_created);
    $created = $platform->getCreatedTime();
    $this->assertEqual($created, $new_created, 'Set name of Ægir platform.');

    $owner_id = $platform->getOwnerId();
    $this->assertEqual($owner_id, $settings['user_id'], 'Get owner of Ægir platform.');

    $new_user = $this->drupalCreateUser([], 'TestUser2');
    $platform->setOwner($new_user);
    $owner_id = $platform->getOwnerId();
    $this->assertEqual($owner_id, $new_user->id(), 'Set owner of Ægir platform.');

    $published = $platform->isPublished();
    $this->assertEqual($published, $settings['status'], 'Get published status of Ægir platform.');

    $unpublished = FALSE;
    $platform->setPublished($unpublished);
    $published = $platform->isPublished();
    $this->assertEqual($published, $unpublished, 'Set published status of Ægir platform.');

    $platform->save();
    $this->drupalGet('admin/aegir/platforms/' . $platform->id());
    $this->assertResponse(200);
    $this->assertText($new_name, 'User with "Platform manager" role can access un-published Ægir platforms.');

  }

}
