<?php

namespace Drupal\aegir_platform\Entity;

use Drupal\aegir_api\Entity\AbstractEntity;

/**
 * Defines an Ægir platform entity.
 *
 * @ingroup aegir_platform
 *
 * @ContentEntityType(
 *   id = "aegir_platform",
 *   label = @Translation("Platform"),
 *   bundle_label = @Translation("Platform type"),
 *   handlers = {
 *     "storage" = "Drupal\aegir_api\Entity\StorageBase",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\ListBuilderBase",
 *     "views_data" = "Drupal\aegir_api\Entity\ViewsDataBase",
 *     "translation" = "Drupal\aegir_api\Entity\TranslationHandlerBase",
 *
 *     "form" = {
 *       "default" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "add" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "edit" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "delete" = "Drupal\aegir_api\Entity\Form\DeleteFormBase",
 *     },
 *     "access" = "Drupal\aegir_api\Entity\AccessControlHandlerBase",
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\HtmlRouteProviderBase",
 *     },
 *   },
 *   base_table = "aegir_platform",
 *   data_table = "aegir_platform_field_data",
 *   revision_table = "aegir_platform_revision",
 *   revision_data_table = "aegir_platform_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer aegir platform entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/platforms/{aegir_platform}",
 *     "add-page" = "/admin/aegir/platforms/add",
 *     "add-form" = "/admin/aegir/platform/add/{aegir_platform_type}",
 *     "edit-form" = "/admin/aegir/platforms/{aegir_platform}/edit",
 *     "delete-form" = "/admin/aegir/platforms/{aegir_platform}/delete",
 *     "version-history" = "/admin/aegir/platforms/{aegir_platform}/revisions",
 *     "revision" = "/admin/aegir/platforms/{aegir_platform}/revisions/{entity_revision}/view",
 *     "revision_revert" = "/admin/aegir/platforms/{aegir_platform}/revisions/{entity_revision}/revert",
 *     "translation_revert" = "/admin/aegir/platforms/{aegir_platform}/revisions/{entity_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/aegir/platforms/{aegir_platform}/revisions/{entity_revision}/delete",
 *     "collection" = "/admin/aegir/platforms",
 *   },
 *   bundle_entity_type = "aegir_platform_type",
 *   field_ui_base_route = "entity.aegir_platform_type.canonical"
 * )
 */
class Entity extends AbstractEntity {

}
