<?php

namespace Drupal\aegir_site\Entity;

use Drupal\aegir_api\Entity\AbstractController;
use Drupal\aegir_api\Entity\EntityInterface;

/**
 * Class Controller.
 *
 * Returns responses for Ægir site routes.
 *
 * @package Drupal\aegir_site\Entity
 */
class Controller extends AbstractController {

  protected $aegirEntityType = 'aegir_site';

  protected $aegirEntityLabel = 'aegir site';

  /**
   * {@inheritdoc}
   */
  public function revisionOverview(EntityInterface $aegir_site) {
    /*
     * *N.B.* This method _must_ override in the parent entity, since it can
     * only accept parameters _named_ after their entity type.
     * @todo: Untangle entity routing to clean this up.
     * @see: \Drupal\aegir_api\Entity\AbstractController::revisionOverview().
     */
    return parent::revisionOverview($aegir_site);
  }

}
