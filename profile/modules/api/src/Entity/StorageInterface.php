<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the storage handler class for Ægir entities.
 *
 * This extends the base storage class, adding required special handling for
 * Ægir entities.
 *
 * @ingroup aegir_api
 */
interface StorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Ægir entity revision IDs for a specific Ægir entity.
   *
   * @param \Drupal\aegir_api\Entity\EntityInterface $entity
   *   The Ægir entity.
   *
   * @return int[]
   *   Ægir entity revision IDs (in ascending order).
   */
  public function revisionIds(EntityInterface $entity);

}
