<?php

namespace Drupal\aegir_api\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines an Ægir entity type.
 */
abstract class AbstractEntityType extends ConfigEntityBundleBase implements EntityTypeInterface {

  /**
   * The Ægir entity type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Ægir entity type label.
   *
   * @var string
   */
  protected $label;

  /**
   * Whether to default to creating new revisions.
   *
   * @var bool
   */
  protected $defaultNewRevision = TRUE;

  /**
   * @inheritdoc
   */
  public function shouldCreateNewRevision() {
    return $this->defaultNewRevision;
  }

}
