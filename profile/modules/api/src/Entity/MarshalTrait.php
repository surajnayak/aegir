<?php

namespace Drupal\aegir_api\Entity;

use Drupal\field\FieldConfigInterface;

trait MarshalTrait {

  /**
   * A flat list of values from any referenced entities.
   */
  protected $marshaledValues = [];

  /**
   * Recurse through entity references, gathering all bundle field values.
   */
  public function marshal() {
    foreach ($this->getBundleFieldDefinitions() as $name => $definition) {
      $field = $this->get($name);
      $type = $definition->getType();
      $this->marshaledValues[$name] =
        $definition->getFieldStorageDefinition()->getCardinality() === 1 ?
          $this->marshalSingleFieldItem($field, $type) :
          $this->marshalMultipleFieldItems($field, $type);
    }
    /* Filter NULL values, which is what entity references will result in. */
    return $this->filterNullValues($this->marshaledValues);
  }

  /**
   * Return an ordered list of task roles.
   *
   * @todo Move to a task order trait?
   * @see \Drupal\aegir_operation\Entity\EntityType.
   */
  public function marshalRoles() {
    $tasks = $this->getBundle()->getOrderedTaskTypes();
    return array_keys($tasks);
  }

  /**
   * Return the bundle for this entity.
   */
  public function getBundle() {
    $bundle_type = $this->getEntityType()->getBundleEntityType();
    $bundle = $this->bundle();
    return \Drupal::entityTypeManager()
      ->getStorage($bundle_type)
      ->load($bundle);
  }

  /**
   * Return a list of all fields attached to this entity via the Field API.
   */
  public function getBundleFieldDefinitions() {
    $fields = $this->getFieldDefinitions();
    foreach ($fields as $name => $field) {
      if (method_exists($field, 'isBaseField') && $field->isBaseField()) {
        unset($fields[$name]);
      }
    }
    return $fields;
  }

  /**
   * Marshal each of a multi-value field's items.
   */
  protected function marshalMultipleFieldItems($items, $type) {
    $values = [];
    foreach ($items as $item) {
      $values[] = $this->marshalSingleFieldItem($item, $type);
    }
    return $values;
  }

  /**
   * Dispatch to the appropriate field-type method.
   */
  protected function marshalSingleFieldItem($item, $type) {
    switch ($type) {
      case 'entity_reference':
      case 'task_reference':
      case 'operation_reference':
        return $this->marshalEntityReferenceField($item);
      case 'string':
      case 'string_long':
      case 'list_string':
        return $this->marshalStringField($item);
      case 'boolean':
        return $this->marshalBooleanField($item);
      case 'link':
        // @TODO Is this the best approach?
        // Ignore fields that are intended to be populated from the backend.
        return;
      default:
        throw new \LogicException(__METHOD__ . " does not currently support '{$type}' fields.");
    }
  }

  /**
   * Recursively marshal each referenced entity.
   */
  protected function marshalEntityReferenceField($item) {
    $entities = $item->referencedEntities();
    foreach ($entities as $entity) {
      /* We append the referenced entity's marshaled values, rather than
       * returning them. This keeps the results to a flat list. */
      $this->marshaledValues += $entity->marshal();
    }
  }

  /**
   * Return the value of a string field.
   */
  protected function marshalStringField($field) {
    return $field->getString();
  }

  /**
   * Return the value of a boolean field.
   */
  protected function marshalBooleanField($field) {
    return (bool) $field->getString();
  }

  /**
   * Filter null values from an array.
   */
  protected function filterNullValues(array $values) {
    return array_filter($values, function($var) { return !is_null($var); } );
  }

}
