<?php

namespace Drupal\aegir_api\Entity\Form;

/**
 * Default form controller for Ægir entity edit forms.
 *
 * @ingroup aegir_api
 */
class EntityFormBase extends AbstractEntityForm {

}
