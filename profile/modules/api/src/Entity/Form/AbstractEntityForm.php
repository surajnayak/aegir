<?php

namespace Drupal\aegir_api\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Ægir entity edit forms.
 *
 * @ingroup aegir_api
 */
abstract class AbstractEntityForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label @entity_type.', [
          '%label' => $entity->label(),
          '@entity_type' => $entity->getEntityType()->getLowercaseLabel(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label @entity_type.', [
          '%label' => $entity->label(),
          '@entity_type' => $entity->getEntityType()->getLowercaseLabel(),
        ]));
    }

    $form_state->setRedirect('entity.' . $entity->getEntityTypeId() . '.canonical', [
      $entity->getEntityTypeId() => $entity->id(),
    ]);
  }

}
