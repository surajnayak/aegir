<?php

namespace Drupal\aegir_api\Command;

use Symfony\Component\Console\Command\Command;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Class AegirCreateCommand.
 *
 * @package Drupal\aegir_queue
 */
abstract class AbstractAegirCommand extends Command {

  use ContainerAwareCommandTrait;

  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    parent::__construct();
  }

  protected function getAegirBundles() {
    $bundles = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      foreach ($this->getBundleInfo($type) as $bundle => $info) {
        $bundles[$type] = [$bundle => $info['label']];
      }
    }
    return $bundles;
  }

  protected function getBundleInfo($type) {
    return \Drupal::service('entity_type.bundle.info')->getBundleInfo($type);
  }

  protected function getAegirEntityTypeLabels() {
    $labels = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      # Cast to string.
      $labels[$type] = "{$entity_type->getLabel()}";
    }
    return $labels;
  }

  protected function getAegirEntityTypes() {
    $types = [];
    foreach ($this->entityTypeManager->getDefinitions() as $type => $entity_type) {
      if ($this->isAnAegirEntityType($type)) {
        $types[$type] = $entity_type;
      }
    }
    return $types;
  }

  protected function isAnAegirEntityType($type) {
    # @TODO Use a class property (or similar) to determine relevant entity
    # types.
    return in_array($type, ['aegir_platform', 'aegir_site']);
  }

  protected function getAegirEntities() {
    $entities = [];
    foreach ($this->getAegirEntityTypes() as $type => $entity_type) {
      $entities[$type] = $this->entityTypeManager->getStorage($type)->loadMultiple();
    }
    return $entities;
  }

}
