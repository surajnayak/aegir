<?php

namespace Drupal\aegir_task\Entity;

use Drupal\aegir_api\Entity\AbstractController;
use Drupal\aegir_api\Entity\EntityInterface;

/**
 * Class Controller.
 *
 * Returns responses for Ægir Task routes.
 *
 * @package Drupal\aegir_task\Entity
 */
class Controller extends AbstractController {

  protected $aegirEntityType = 'aegir_task';

  protected $aegirEntityLabel = 'aegir task';

  /**
   * {@inheritdoc}
   */
  public function revisionOverview(EntityInterface $aegir_task) {
    /*
     * *N.B.* This method _must_ override in the parent entity, since it can
     * only accept parameters _named_ after their entity type.
     * @todo: Untangle entity routing to clean this up.
     * @see: \Drupal\aegir_api\Entity\AbstractController::revisionOverview().
     */
    return parent::revisionOverview($aegir_task);
  }

}
