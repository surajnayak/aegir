<?php

namespace Drupal\aegir_task\Entity;

use Drupal\aegir_api\Entity\AbstractEntity;

/**
 * Defines the Ægir Task entity.
 *
 * @ingroup aegir_task
 *
 * @ContentEntityType(
 *   id = "aegir_task",
 *   label = @Translation("Task"),
 *   bundle_label = @Translation("Task type"),
 *   handlers = {
 *     "storage" = "Drupal\aegir_api\Entity\StorageBase",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\aegir_api\Entity\ListBuilderBase",
 *     "views_data" = "Drupal\aegir_api\Entity\ViewsDataBase",
 *     "translation" = "Drupal\aegir_api\Entity\TranslationHandlerBase",
 *
 *     "form" = {
 *       "default" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "add" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "edit" = "Drupal\aegir_api\Entity\Form\EntityFormBase",
 *       "delete" = "Drupal\aegir_api\Entity\Form\DeleteFormBase",
 *     },
 *     "inline_form" = "Drupal\aegir_task\Entity\Form\TaskInlineForm",
 *
 *     "access" = "Drupal\aegir_api\Entity\AccessControlHandlerBase",
 *     "route_provider" = {
 *       "html" = "Drupal\aegir_api\Entity\HtmlRouteProviderBase",
 *     },
 *   },
 *   base_table = "aegir_task",
 *   data_table = "aegir_task_field_data",
 *   revision_table = "aegir_task_revision",
 *   revision_data_table = "aegir_task_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer aegir task entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_user",
 *     "revision_created" = "revision_created",
 *     "revision_log_message" = "revision_log_message",
 *   },
 *   links = {
 *     "canonical" = "/admin/aegir/tasks/{aegir_task}",
 *     "add-page" = "/admin/aegir/tasks/add",
 *     "add-form" = "/admin/aegir/task/add/{aegir_task_type}",
 *     "edit-form" = "/admin/aegir/tasks/{aegir_task}/edit",
 *     "delete-form" = "/admin/aegir/tasks/{aegir_task}/delete",
 *     "version-history" = "/admin/aegir/tasks/{aegir_task}/revisions",
 *     "revision" = "/admin/aegir/tasks/{aegir_task}/revisions/{entity_revision}/view",
 *     "revision_revert" = "/admin/aegir/tasks/{aegir_task}/revisions/{entity_revision}/revert",
 *     "translation_revert" = "/admin/aegir/tasks/{aegir_task}/revisions/{entity_revision}/revert/{langcode}",
 *     "revision_delete" = "/admin/aegir/tasks/{aegir_task}/revisions/{entity_revision}/delete",
 *     "collection" = "/admin/aegir/tasks",
 *   },
 *   bundle_entity_type = "aegir_task_type",
 *   field_ui_base_route = "entity.aegir_task_type.canonical"
 * )
 */
class Entity extends AbstractEntity {

}
