<?php

namespace Drupal\aegir_task\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\aegir_task\Entity\Entity as Task;

/**
 * Test to exercise API functions not covered by other tests.
 *
 * @group aegir
 */
class ApiTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'aegir_test_task',
  ];

  public $profile = 'aegir';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Disable schema validation for the base theme.
   *
   * @see: https://www.drupal.org/node/2860072
   */
  protected function getConfigSchemaExclusions() {
    return array_merge(parent::getConfigSchemaExclusions(), ['bootstrap.settings']);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([], 'TestUser');
    $this->user->addRole('aegir_task_manager');
    $this->user->save();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testApiMethods() {
    $settings = [
      // Use one of the bundles exported to the 'aegir_test_task' module.
      'type' => 'test_task_type_1',
      'name' => 'TEST_TASK_A',
      'created' => time(),
      'user_id' => $this->rootUser->id(),
      'status' => 1,
    ];
    $task = Task::create($settings);

    $type = $task->getType();
    $this->assertEqual($type, $settings['type'], 'Get type (bundle) of Ægir task.');

    $name = $task->getName();
    $this->assertEqual($name, $settings['name'], 'Get name of Ægir task.');

    $new_name = 'TEST_TASK_B';
    $task->setName($new_name);
    $name = $task->getName();
    $this->assertEqual($name, $new_name, 'Set name of Ægir task.');

    $created = $task->getCreatedTime();
    $this->assertEqual($created, $settings['created'], 'Get creation time of Ægir task.');

    $new_created = time() - 10;
    $task->setCreatedTime($new_created);
    $created = $task->getCreatedTime();
    $this->assertEqual($created, $new_created, 'Set name of Ægir task.');

    $owner_id = $task->getOwnerId();
    $this->assertEqual($owner_id, $settings['user_id'], 'Get owner of Ægir task.');

    $new_user = $this->drupalCreateUser([], 'TestUser2');
    $task->setOwner($new_user);
    $owner_id = $task->getOwnerId();
    $this->assertEqual($owner_id, $new_user->id(), 'Set owner of Ægir task.');

    $published = $task->isPublished();
    $this->assertEqual($published, $settings['status'], 'Get published status of Ægir task.');

    $unpublished = FALSE;
    $task->setPublished($unpublished);
    $published = $task->isPublished();
    $this->assertEqual($published, $unpublished, 'Set published status of Ægir task.');

    $task->save();
    $this->drupalGet('admin/aegir/tasks/' . $task->id());
    $this->assertResponse(200);
    $this->assertText($new_name, 'User with "Task manager" role can access un-published Ægir tasks.');

  }

}
