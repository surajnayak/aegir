<?php

namespace Drupal\aegir_operation\Plugin\views\field;

use Drupal\Core\Url;
use Drupal\views\ResultRow;
use Drupal\views\Plugin\views\field\EntityLink;

/**
 * Base class for field handler to present operation buttons.
 *
 * @ingroup views_field_handlers
 */
abstract class AbstractOperationButton extends EntityLink {

  /**
   * The route to trigger the ajax modal command.
   */
  protected $modalRoute = NULL;

  /**
   * Return URL classes to enable a modal dialog.
   */
  protected $modalUrlClasses = [
    'use-ajax',
    'button',
    'operation-button',
  ];

  /**
   * Return the route to trigger the ajax modal command.
   */
  protected function getModalRoute() {
    return $this->modalRoute;
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlInfo(ResultRow $row) {
    return $this
      ->getModalUrl($row->_entity)
      ->setOptions($this->getModalUrlOptions());
  }

  /**
   * {@inheritdoc}
   */
  protected function renderLink(ResultRow $row) {
    $text = parent::renderLink($row);
    if ($this->buttonIsEnabled($row)) {
      return $text;
    }
    return $this->disabledButton($text);
  }

  /**
   * Determine whether a button should be enabled or disabled.
   *
   * Override in subclasses to test for appropriate circumstances.
   */
  protected function buttonIsEnabled(ResultRow $row) {
    return TRUE;
  }

  /**
   * Return a disabled button.
   */
  protected function disabledButton(string $text) {
    $this->options['alter']['make_link'] = FALSE;

    $classes = $this->getModalUrlClasses();
    $classes[] = 'is-disabled';
    $classes = implode(' ', $classes);

    return "<span class='{$classes}'>{$text}</span>";
  }

  /**
   * Return the URL that will return an AJAX modal dialog command.
   */
  protected function getModalUrl($entity) {
    $route = $this->getModalRoute();
    $uuid = $entity->uuid();
    return Url::fromRoute($route, ['operation' => $uuid]);
  }

  /**
   * Return URL options to enable a modal dialog.
   */
  protected function getModalUrlOptions() {
    return [
      'attributes' => [
        'class' => $this->getModalUrlClasses(),
        'data-dialog-type' => 'modal',
      ],
    ];
  }

  /**
   * Return URL classes to enable a modal dialog.
   */
  protected function getModalUrlClasses() {
    return $this->modalUrlClasses;
  }

}
