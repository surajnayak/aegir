<?php

namespace Drupal\aegir_operation\Plugin\EntityReferenceSelection;

use Drupal\aegir_api\Plugin\Field\EntityReferenceSelection\AbstractSelection;

/**
 * Provides specific access control for the user entity type.
 *
 * @EntityReferenceSelection(
 *   id = "default:aegir_operation",
 *   label = @Translation("Operation selection"),
 *   entity_types = {"aegir_operation"},
 *   group = "default",
 *   weight = 0
 * )
 */
class OperationSelection extends AbstractSelection {

  /**
   * {@inheritdoc}
   */
  protected $bundleListTitle = 'Operation type';

}
