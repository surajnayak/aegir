<?php

namespace Drupal\aegir_operation\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Command\Command;
use Drupal\Console\Annotations\DrupalCommand;
use Drupal\Console\Core\Command\Shared\ContainerAwareCommandTrait;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Core\Entity\EntityRepository;

/**
 * Class AegirInputCommand.
 *
 * @package Drupal\aegir_operation
 *
 * @DrupalCommand (
 *   extension="aegir_operation",
 *   extensionType="module"
 * )
 */
class AegirInputCommand extends Command {

  use ContainerAwareCommandTrait;

  /**
   * The EntityRepository service.
   *
   * @var EntityRepository
   */
  protected $entityRepository;

  /**
   * Constructs a new AegirInputCommand object.
   */
  public function __construct(EntityRepository $entity_repository) {
    $this->entityRepository = $entity_repository;
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('aegir:input')
      ->setDescription($this->trans('commands.aegir.input.description'))
      ->addArgument(
        'type',
        InputArgument::REQUIRED,
        $this->trans('commands.aegir.input.arguments.type')
      )
      ->addArgument(
        'uuid',
        InputArgument::REQUIRED,
        $this->trans('commands.aegir.input.arguments.uuid')
      )
      ->addArgument(
        'field',
        InputArgument::REQUIRED,
        $this->trans('commands.aegir.input.arguments.field')
      )
       ->addArgument(
        'data',
        InputArgument::REQUIRED,
        $this->trans('commands.aegir.input.arguments.data')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);
    $args = $input->getArguments();
    foreach ($args as $key => $value) {
      $io->info($key . ': ' . $value);
    }
    $entity = $this->entityRepository->loadEntityByUuid($args['type'], $args['uuid']);
    if (is_null($entity)) return $io->error(sprintf(
      $this->trans('commands.aegir.input.errors.invalid-uuid'),
      $args['type'],
      $args['uuid']
    ));
    if (!$entity->hasField($args['field'])) return $io->error(sprintf(
      $this->trans('commands.aegir.input.errors.invalid-field'),
      $args['field'],
      $args['type']
    ));
    $entity->set($args['field'], $args['data']);
    $entity->createNewRevision(FALSE);
    $entity->save();
  }

}
