<?php

namespace Drupal\aegir_operation;

use Drupal\aegir_operation\Entity\Entity as OperationEntity;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaskLog {

  use StringTranslationTrait;

  /**
   * Request parameters required for successful processing of task log output.
   */
  protected $requiredOutputParams = [
    'sequence',
    'task_log',
    'timestamp',
  ];

  /**
   * Request parameters required for successful processing of task exit code.
   */
  protected $requiredExitCodeParams = [
    'exitcode',
  ];

  /**
   * Construct an Ægir TaskLog object.
   *
   * @todo Ensure only task entities are passed in (via a TaskEntityInterface?)
   */
  public function __construct() {
    $this->log = \Drupal::logger('aegir_task_log');
  }

  /**
   * Processes a task log output post.
   *
   * @see aegir_operation.routing.yml
   */
  public function receiveOutput(OperationEntity $operation, Request $request) {
    $response = new Response;

    $params = $this->getParams($request, $this->requiredOutputParams);
    if (!$params) return $response->setStatusCode(404);

    $operation->task_log_output[] = $params['task_log'];
    $operation->task_log_sequence[] = $params['sequence'];
    $operation->task_log_timestamp[] = $params['timestamp'];
    // Don't create a new revision for each line of the log output.
    $operation->createNewRevision(FALSE);
    $operation->save();

    return $response->setStatusCode(200);
  }

  /**
   * Processes a task exit code post.
   *
   * @see aegir_operation.routing.yml
   */
  public function receiveExitCode(OperationEntity $operation, Request $request) {
    $response = new Response;

    $params = $this->getParams($request, $this->requiredExitCodeParams);
    if (!$params) return $response->setStatusCode(404);

    $this->log->notice($this->t('Updating status of :label operation.', [':label' => $operation->label()]));
    $operation->task_exitcode = $params['exitcode'];
    $operation->updateStatus();
    // Don't create a new revision when updating the task exit code.
    $operation->createNewRevision(FALSE);
    $operation->save();

    $referer = $operation->getReferencingEntity();
    $this->log->notice($this->t('Updating status of :label :type.', [
      ':label' => $referer->label(),
      ':type' => $referer->getEntityType()->getLowercaseLabel(),
    ]));
    $referer->operation_status = $operation->operation_status;
    // @TODO Figure out whether to create a new revision of the refering entity.
    #$operation->createNewRevision(FALSE);
    $referer->save();

    return $response->setStatusCode(200);
  }

  /**
   * Return valid parameters.
   */
  protected function getParams(Request $request, array $required) {
    $params = $this->getRawParams($request);
    return $this->validateParams($params, $required);
  }

  /**
   * Return request paramaters.
   */
  protected function getRawParams(Request $request) {
    return $request->request->all();
  }

  /**
   * Return parameters, ensuring that required ones exist.
   */
  protected function validateParams(array $params, array $required) {
    foreach ($required as $param) {
      if (!array_key_exists($param, $params)) {
        $this->log->error($this->t('Failed to validate parameters. Missing: @param.', ['@param' => $param]));
        return FALSE;
      }
    }
    return $params;
  }

}
