<?php

namespace Drupal\aegir_example\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\aegir_example\Entity\Entity as Example;

/**
 * Test to exercise API functions not covered by other tests.
 *
 * @group aegir
 */
class ApiTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'aegir_test_example',
  ];

  public $profile = 'aegir';

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Disable schema validation for the base theme.
   *
   * @see: https://www.drupal.org/node/2860072
   */
  protected function getConfigSchemaExclusions() {
    return array_merge(parent::getConfigSchemaExclusions(), ['bootstrap.settings']);
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->user = $this->drupalCreateUser([], 'TestUser');
    $this->user->addRole('aegir_example_manager');
    $this->user->save();
    $this->drupalLogin($this->user);
  }

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testApiMethods() {
    $settings = [
      // Use one of the bundles exported to the 'aegir_test_example' module.
      'type' => 'test_example_type_1',
      'name' => 'TEST_EXAMPLE_A',
      'created' => time(),
      'user_id' => $this->rootUser->id(),
      'status' => 1,
    ];
    $example = Example::create($settings);

    $type = $example->getType();
    $this->assertEqual($type, $settings['type'], 'Get type (bundle) of Ægir example.');

    $name = $example->getName();
    $this->assertEqual($name, $settings['name'], 'Get name of Ægir example.');

    $new_name = 'TEST_EXAMPLE_B';
    $example->setName($new_name);
    $name = $example->getName();
    $this->assertEqual($name, $new_name, 'Set name of Ægir example.');

    $created = $example->getCreatedTime();
    $this->assertEqual($created, $settings['created'], 'Get creation time of Ægir example.');

    $new_created = time() - 10;
    $example->setCreatedTime($new_created);
    $created = $example->getCreatedTime();
    $this->assertEqual($created, $new_created, 'Set name of Ægir example.');

    $owner_id = $example->getOwnerId();
    $this->assertEqual($owner_id, $settings['user_id'], 'Get owner of Ægir example.');

    $new_user = $this->drupalCreateUser([], 'TestUser2');
    $example->setOwner($new_user);
    $owner_id = $example->getOwnerId();
    $this->assertEqual($owner_id, $new_user->id(), 'Set owner of Ægir example.');

    $published = $example->isPublished();
    $this->assertEqual($published, $settings['status'], 'Get published status of Ægir example.');

    $unpublished = FALSE;
    $example->setPublished($unpublished);
    $published = $example->isPublished();
    $this->assertEqual($published, $unpublished, 'Set published status of Ægir example.');

    $example->save();
    $this->drupalGet('admin/aegir/examples/' . $example->id());
    $this->assertResponse(200);
    $this->assertText($new_name, 'User with "Example manager" role can access un-published Ægir examples.');

  }

}
