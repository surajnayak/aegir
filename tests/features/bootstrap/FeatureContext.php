<?php

# TODO: Make Drumkit CLI context available for use via Composer.
include dirname(dirname(dirname(dirname(__FILE__)))) . "/.mk/features/bootstrap/Drumkit/DrumkitContext.php";

use Drumkit\DrumkitContext;
use Behat\Behat\Hook\Scope\AfterFeatureScope;

class FeatureContext extends DrumkitContext {

  /**
   * @AfterFeature
   */
  public static function cleanEntities(AfterFeatureScope $scope) {

    # All test entities should be prefixed for easy identification.
    $prefix = 'TEST_';

    # List of entity and bundle types to clean up after.
    $types = [
      'operations' => [
        'entity_type' => 'aegir_operation_type',
        'entity' => 'aegir_operation',
        'test_modules' => [
          'aegir_test_operation',
          'content_translation',
          'language',
        ],
      ],
      'tasks' => [
        'entity_type' => 'aegir_task_type',
        'entity' => 'aegir_task',
        'test_modules' => [
          'aegir_test_task',
          'content_translation',
          'language',
        ],
      ],
    ];

    $tags = $scope->getFeature()->getTags();
    foreach ($tags as $tag) {
      if (array_key_exists($tag, $types)) {
        print("Cleaning up test '{$tag}' entities and bundles.\n");
        $data = $types[$tag];
        $query = \Drupal::entityQuery($data['entity_type']);
        $bundles = $query
          ->condition('label', $prefix, 'STARTS_WITH')
          ->execute();
        foreach ($bundles as $bundle_label) {
          $query = \Drupal::entityQuery($data['entity']);
          $entity_ids = $query
            ->condition('type', $bundle_label)
            ->condition('name', $prefix, 'STARTS_WITH')
            ->execute();
          $entities = \Drupal::entityTypeManager()
            ->getStorage($data['entity'])
            ->loadMultiple($entity_ids);
          foreach ($entities as $id => $entity) {
            print('  Deleting ' . $entity->label() . ' ' . $entity->getEntityType()->getLabel() . ' (bundle: ' . $bundle_label . ")\n");
            $entity->delete();
          }
          $entity_update_manager = \Drupal::entityDefinitionUpdateManager();
          $entity_type = $entity_update_manager->getEntityType($data['entity_type']);
          if ($entity_type) {
            print('  Deleting ' . $bundle_label . ' ' . $entity_type->getLabel() . "\n");
            $entity_update_manager->uninstallEntityType($entity_type);
          }
        }
        print('Disabling test module(s): ' . implode(',', $data['test_modules']) . "\n");
        \Drupal::service('module_installer')->uninstall($data['test_modules']);
      }
    }
    if (in_array('l10n', $tags)) {
      print("Disabling languages.\n");
      Drupal::configFactory()->getEditable('language.entity.fr')->delete();
      print("Rebuilding caches.\n");
      drupal_flush_all_caches();
    }
  }

  /**
   * @Given I am at :path on the command line
   */
  public function iAmAtOnTheCommandLine($path)
  {
    chdir($path);
  }

}
