#!/bin/bash

shopt -s extglob

# Recreate 'optional' config dirs.
for module in profile/modules/*; do
  echo "Operating in $module:"
  cd $module
  echo "Creating 'optional' config directory."
  mkdir -p config/optional;
  echo "Moving views config."
  mv config/install/views.view.* config/optional/
  echo "Moving blocks config."
  mv config/install/block.block.* config/optional/
  echo "Moving menu links config."
  mv config/install/menu_link_config.menu_link_config.* config/optional/
  echo "Resetting dependencies."
  git checkout *.info.yml
  cd -
done


