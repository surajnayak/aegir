#!/bin/sh -eux

# Update the package list
apt-get -y update > /dev/null

# Keep package installations to the minimum
cat > /etc/apt/apt.conf.d/01norecommend << EOF
APT::Install-Recommends "0";
APT::Install-Suggests "0";
EOF

apt-get -y -qq install apt-utils apt-transport-https ca-certificates dialog readline-common
