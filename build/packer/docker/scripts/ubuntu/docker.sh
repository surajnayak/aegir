#!/bin/sh -eux

# Install Docker from official repo.
apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D > /dev/null
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" > /etc/apt/sources.list.d/docker.list
apt-get update > /dev/null
apt-get -y -qq install docker-engine >/dev/null
service docker start
