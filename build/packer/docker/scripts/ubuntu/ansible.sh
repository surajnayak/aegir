#!/bin/sh -eux

# Install the latest version of Ansible.
apt-get -yqq install python-pip build-essential libssl-dev libffi-dev python-dev
# Pin pip to avoid https://github.com/pypa/pip/issues/5240
#pip install --upgrade pip
pip install --upgrade pip==9.0.3
pip install setuptools
pip install ansible

# Install an init system, to allow services to run.
apt-get -yqq install systemd-sysv

