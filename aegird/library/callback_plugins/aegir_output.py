#!/usr/bin/python

# Make coding more python3-ish
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

from ansible import constants as C
from ansible.playbook.task_include import TaskInclude
from ansible.plugins.callback.default import CallbackModule as CallbackModule_default
from ansible.utils.color import colorize, stringc

class CallbackModule(CallbackModule_default):
    '''
    Reference: https://github.com/ansible/ansible/blob/devel/lib/ansible/plugins/callback/default.py
    '''
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = 'stdout'
    CALLBACK_NAME = 'aegir_output'
    CALLBACK_NEEDS_WHITELIST = True

    def hostcolor(self, host, stats, color=True):
        if color:
            if stats['failures'] != 0 or stats['unreachable'] != 0:
                return u"%-37s" % stringc(host, C.COLOR_ERROR)
            elif stats['warnings'] != 0:
                return u"%-37s" % stringc(host, C.COLOR_WARN)
            elif stats['changed'] != 0:
                return u"%-37s" % stringc(host, C.COLOR_CHANGED)
            else:
                return u"%-37s" % stringc(host, C.COLOR_OK)
        return u"%-26s" % host

    def v2_playbook_on_stats(self, stats):
        '''
        Override the default callback to properly display warning recap stats.
        '''

        self._display.banner("PLAY RECAP")

        hosts = sorted(stats.processed.keys())
        for h in hosts:
            t = stats.summarize(h)

            # Set a default value for 'warnings', since it isn't a standard stat.
            w = {'warnings': 0}
            for k in sorted(stats.custom.keys()):
                if str(k) == h:
                    w = {'warnings': stats.custom[k].pop('_warnings')}
                # Clean up empty arrays, if no other stats were registered.
                if not len(stats.custom[k]):
                    stats.custom.pop(k)
            t.update(w)

            self._display.display(u"%s : %s %s %s %s %s" % (
                self.hostcolor(h, t),
                colorize(u'ok', t['ok']-t['warnings'], C.COLOR_OK),
                colorize(u'changed', t['changed'], C.COLOR_CHANGED),
                colorize(u'warnings', t['warnings'], C.COLOR_WARN),
                colorize(u'unreachable', t['unreachable'], C.COLOR_UNREACHABLE),
                colorize(u'failed', t['failures'], C.COLOR_ERROR)),
                screen_only=True
            )

            self._display.display(u"%s : %s %s %s %s %s" % (
                self.hostcolor(h, t, False),
                colorize(u'ok', t['ok']-t['warnings'], None),
                colorize(u'changed', t['changed'], None),
                colorize(u'warnings', t['warnings'], None),
                colorize(u'unreachable', t['unreachable'], None),
                colorize(u'failed', t['failures'], None)),
                log_only=True
            )

        self._display.display("", screen_only=True)

        # print custom stats
        if self._plugin_options.get('show_custom_stats', C.SHOW_CUSTOM_STATS) and stats.custom:  # fallback on constants for inherited plugins missing docs
            self._display.banner("CUSTOM STATS: ")
            # per host
            # TODO: come up with 'pretty format'
            for k in sorted(stats.custom.keys()):
                if k == '_run':
                    continue
                self._display.display('\t%s: %s' % (k, self._dump_results(stats.custom[k], indent=1).replace('\n', '')))

            # print per run custom stats
            if '_run' in stats.custom:
                self._display.display("", screen_only=True)
                self._display.display('\tRUN: %s' % self._dump_results(stats.custom['_run'], indent=1).replace('\n', ''))
            self._display.display("", screen_only=True)


    def v2_runner_on_ok(self, result):
        '''
        Override the default callback to properly display warnings.
        '''

        delegated_vars = result._result.get('_ansible_delegated_vars', None)
        self._clean_results(result._result, result._task.action)

        if self._play.strategy == 'free' and self._last_task_banner != result._task._uuid:
            self._print_task_banner(result._task)

        if isinstance(result._task, TaskInclude):
            return
        elif result._result.get('warning', False):
            if delegated_vars:
                msg = "warning: [%s -> %s]" % (result._host.get_name(), delegated_vars['ansible_host'])
            else:
                msg = "warning: [%s]" % result._host.get_name()
            color = C.COLOR_WARN
        elif result._result.get('changed', False):
            if delegated_vars:
                msg = "changed: [%s -> %s]" % (result._host.get_name(), delegated_vars['ansible_host'])
            else:
                msg = "changed: [%s]" % result._host.get_name()
            color = C.COLOR_CHANGED
        else:
            if delegated_vars:
                msg = "ok: [%s -> %s]" % (result._host.get_name(), delegated_vars['ansible_host'])
            else:
                msg = "ok: [%s]" % result._host.get_name()
            color = C.COLOR_OK

        self._handle_warnings(result._result)

        if result._task.loop and 'results' in result._result:
            self._process_items(result)
        else:

            if (self._display.verbosity > 0 or '_ansible_verbose_always' in result._result) and '_ansible_verbose_override' not in result._result:
                if result._task.action == 'debug' and 'changed' in result._result:
                    del result._result['changed']
                msg += " => %s" % (self._dump_results(result._result),)
            self._display.display(msg, color=color)

    def v2_runner_item_on_ok(self, result):
        '''
        Override the default callback to properly display warnings.
        '''

        delegated_vars = result._result.get('_ansible_delegated_vars', None)
        self._clean_results(result._result, result._task.action)
        if isinstance(result._task, TaskInclude):
            return
        elif result._result.get('warning', False):
            msg = 'warning'
            color = C.COLOR_WARN
        elif result._result.get('changed', False):
            msg = 'changed'
            color = C.COLOR_CHANGED
        else:
            msg = 'ok'
            color = C.COLOR_OK

        if delegated_vars:
            msg += ": [%s -> %s]" % (result._host.get_name(), delegated_vars['ansible_host'])
        else:
            msg += ": [%s]" % result._host.get_name()

        msg += " => (item=%s)" % (self._get_item(result._result),)

        if (self._display.verbosity > 0 or '_ansible_verbose_always' in result._result) and '_ansible_verbose_override' not in result._result:
            msg += " => %s" % self._dump_results(result._result)
        self._display.display(msg, color=color)

