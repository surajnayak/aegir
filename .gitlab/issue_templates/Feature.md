User Story
----------

```gherkin
Feature: <Document feature requests as a user story>
  In order to <ensure that I understand a feature specification>,
  as a <developer>,
  I want to be able to <refer a description how a feature ought to work>.

  Scenario: <Describe how to interact with the feature in question>
    Given I <create an issue in GitLab>
     Then I should get "<a user story template>"

...
```

Completion Checklist
--------------------

- [ ] Tests are implemented and passing
- [ ] Code coverage remains 100%
- [ ] Linting passes
- [ ] Code review by core team member
